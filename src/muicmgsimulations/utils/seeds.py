class SeedRepository:

    def __init__(self, init_seed):
        self.next_seed = init_seed

    def next(self):
        tmp = self.next_seed

        self.next_seed += 1

        return tmp
