machines = {
    'LHmuC': {'Emu': 1500, 'Ep': 7000},
    'MuIC2': {'Emu': 960, 'Ep': 1000},
    'MuIC': {'Emu': 960, 'Ep': 275},
    'HERA': {'Emu': 27.5, 'Ep': 920},
}
