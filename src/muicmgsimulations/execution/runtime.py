import os

# GLOBAL
project_root = None
project_name = None
debug_en = False

# SEEDS
seeds = None
output_dir = None


# RESOURCES
def resource(path):
    return os.path.join(project_root, 'rsc', path)
