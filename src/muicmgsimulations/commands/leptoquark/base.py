model_commands = {
    's3': [
        'import model LO_LQ_S3',
        'define p = p b b~',
        'define j = p b b~',
    ]
}

base_config = {
    'nevents': 50000,
    'lpp1': 0,
    'lpp2': 0,
    'ebeam1': 0,
    'ebeam2': 0,
    'polbeam1': 0,
    'polbeam2': 0,
    # DISABLE SYSTEMATICS
    'use_syst': False,
    # DISABLE FUDICIAL CUTS
    'ptj': 20,
    'ptl': 10,
    'ptjmax': -1,
    'ptlmax': -1,
    'pt_min_pdg': '{}',
    'pt_max_pdg': '{}',
    'etaj': 5,
    'etal': 2.5,
    'etalmin': 0,
    'eta_min_pdg': '{}',
    'eta_max_pdg': '{}',
    'drjl': 0.4,
    'drjlmax': -1,
    'ptheavy': 0,
    'maxjetflavor': 5,
    # LEPTOQUARK
    'MS3': 1000,
    'yLL1x1': 0,
    'yLL1x2': 0,
    'yLL1x3': 0,
    'yLL2x1': 0,
    'yLL2x2': 0,
    'yLL2x3': 0,
    'yLL3x1': 0,
    'yLL3x2': 0.1,
    'yLL3x3': 0
}
