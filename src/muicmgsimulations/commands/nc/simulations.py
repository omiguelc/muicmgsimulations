from muicmgsimulations.commands.nc.base import model_commands, base_config
from muicmgsimulations.execution import runtime
from muicmgsimulations.utils.constants import machines

processes = [
    {'name': 'nc_bj', 'title': 'NC to b-jet',
     'commands': ['define p = b b~', 'define j = b b~', 'generate p mu- > j mu-'], 'model': '5-sm'},
    {'name': 'nc_cj', 'title': 'NC to c-jet',
     'commands': ['define p = c c~', 'define j = c c~', 'generate p mu- > j mu-'], 'model': '5-sm'},
    {'name': 'nc_lj', 'title': 'NC to light jets',
     'commands': ['define p = u u~ d d~ s s~', 'define j = u u~ d d~ s s~', 'generate p mu- > j mu-'], 'model': '5-sm'},
]


def generate_process_generators(output_dir):
    # GENERATE PROCESSES
    for process in processes:
        print('Generating commands for %s' % process['title'])

        commands = list()

        # APPEND FLAVOR SUPPORT
        commands.extend(model_commands.get(process['model'], list()))

        # APPEND PROCESS
        commands.extend(process['commands'])

        # APPEND OUTPUT
        commands.append('output %s\n' % process['name'])

        # BUILD INPUT
        commands_output = ''

        for command in commands:
            commands_output += '%s%s' % ('' if commands_output == '' else '\n', command)

        with open('%s/generate_process_%s.txt' % (output_dir, process['name']), 'w') as f:
            f.write(commands_output)

        print('Done: %s\n' % process['name'])


def generate_long_runs(output_dir):
    process_list = ['nc_bj', 'nc_cj', 'nc_lj']
    machine_list = ['LHmuC', 'MuIC2', 'MuIC']

    for machine_name in machine_list:
        machine = machines[machine_name]

        for process_name in process_list:
            run_name = '%s' % machine_name
            filename = '%s_%s' % (process_name, run_name)

            print('Generating %s' % filename)

            commands_output = 'set automatic_html_opening False\n'
            commands_output += 'launch %s -n %s\n0\n' % (process_name, run_name)

            config = base_config.copy()
            config['iseed'] = runtime.seeds.next()
            config['nevents'] = 150000
            config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
            config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
            config['ebeam1'] = machine['Ep']
            config['ebeam2'] = machine['Emu']
            config['polbeam1'] = 0
            config['polbeam2'] = 0
            config['dynamical_scale_choice'] = 0

            for key, value in config.items():
                commands_output += 'set %s %s\n' % (key, value)

            commands_output += '0\n'

            with open('%s/run_%s.txt' % (output_dir, filename), 'w') as f:
                f.write(commands_output)

            print('Done: %s\n' % filename)
