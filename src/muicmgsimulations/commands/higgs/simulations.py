from muicmgsimulations.commands.higgs.base import model_commands, base_config
from muicmgsimulations.execution import runtime
from muicmgsimulations.utils.constants import machines

processes = [
    {'name': 'nc', 'title': 'VBF Higgs NC',
     'commands': ['generate p mu- > j mu- h'], 'model': '5-sm'},
    {'name': 'cc', 'title': 'VBF Higgs CC',
     'commands': ['generate p mu- > j vm h'], 'model': '5-sm'},
    {'name': 'nc_cc_to_bb', 'title': 'VBF Higgs NC+CC to bb~',
     'commands': ['generate p mu- > j mu- h, h > b b~', 'generate p mu- > j vm h, h > b b~'], 'model': '5-heft'}
]

polarizations = [
    {'name': 'Pmu_eqm100', 'Pp': 0, 'Pmu': -100},
    {'name': 'Pmu_eqm40', 'Pp': 0, 'Pmu': -40},
    {'name': 'Pmu_eqm20', 'Pp': 0, 'Pmu': -20},
    {'name': 'Pmu_eqm10', 'Pp': 0, 'Pmu': -10},
    {'name': 'Pmu_eq0', 'Pp': 0, 'Pmu': 0},
    {'name': 'Pmu_eq10', 'Pp': 0, 'Pmu': 10},
    {'name': 'Pmu_eq20', 'Pp': 0, 'Pmu': 20},
    {'name': 'Pmu_eq40', 'Pp': 0, 'Pmu': 40},
    {'name': 'Pmu_eq100', 'Pp': 0, 'Pmu': 100},
]


def generate_process_generators(output_dir):
    # GENERATE PROCESSES
    for process in processes:
        print('Generating commands for %s' % process['title'])

        commands = list()

        # APPEND FLAVOR SUPPORT
        commands.extend(model_commands.get(process['model'], list()))

        # APPEND PROCESS
        commands.extend(process['commands'])

        # APPEND OUTPUT
        commands.append('output %s\n' % process['name'])

        # BUILD INPUT
        commands_output = ''
        for command in commands:
            commands_output += '%s%s' % ('' if commands_output == '' else '\n', command)

        with open('%s/generate_process_%s.txt' % (output_dir, process['name']), 'w') as f:
            f.write(commands_output)

        print('Done: %s\n' % process['name'])


def generate_machine_comparison_runs(output_dir):
    process_list = ['nc', 'cc']
    machine_list = ['LHmuC', 'MuIC2', 'MuIC', 'HERA']

    for machine_name in machine_list:
        machine = machines[machine_name]

        for process_name in process_list:
            run_name = '%s' % machine_name
            filename = 'machine_comparison_%s_%s' % (process_name, run_name)

            print('Generating %s' % filename)

            commands_output = 'set automatic_html_opening False\n'
            commands_output += 'launch %s -n %s\n0\n' % (process_name, run_name)

            config = base_config.copy()
            config['iseed'] = runtime.seeds.next()
            config['nevents'] = 50000
            config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
            config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
            config['ebeam1'] = machine['Ep']
            config['ebeam2'] = machine['Emu']

            for key, value in config.items():
                commands_output += 'set %s %s\n' % (key, value)

            commands_output += '0\n'

            with open('%s/run_%s.txt' % (output_dir, filename), 'w') as f:
                f.write(commands_output)

            print('Done: %s\n' % filename)


def generate_polarization_scan_runs(output_dir):
    process_list = ['nc_cc_to_bb']
    machine_list = ['MuIC']

    for machine_name in machine_list:
        machine = machines[machine_name]

        for process_name in process_list:
            for polarization in polarizations:
                run_name = '%s_%s' % (machine_name, polarization['name'])
                filename = 'polarization_scan_%s_%s' % (process_name, run_name)

                print('Generating %s' % filename)

                commands_output = 'set automatic_html_opening False\n'
                commands_output += 'launch %s -n %s\n0\n' % (process_name, run_name)

                config = base_config.copy()
                config['iseed'] = runtime.seeds.next()
                config['nevents'] = 50000
                config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
                config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
                config['ebeam1'] = machine['Ep']
                config['ebeam2'] = machine['Emu']
                config['polbeam1'] = polarization['Pp']
                config['polbeam2'] = polarization['Pmu']
                config['mmbb'] = 50
                config['mmbbmax'] = 170
                config['ptb'] = 20
                config['etab'] = 6
                config['etabmin'] = -6

                for key, value in config.items():
                    commands_output += 'set %s %s\n' % (key, value)

                commands_output += '0\n'

                with open('%s/run_%s.txt' % (output_dir, filename), 'w') as f:
                    f.write(commands_output)

                print('Done: %s\n' % filename)


def generate_pythia_runs(output_dir):
    process_list = ['nc_cc_to_bb']
    machine_list = ['MuIC']
    polarization_list = [polarization['name'] for polarization in polarizations]

    for machine_name in machine_list:
        for process_name in process_list:
            for polarization_name in polarization_list:
                run_name = '%s_%s' % (machine_name, polarization_name)
                filename = 'polarization_scan_%s_%s' % (process_name, run_name)

                print('Generating %s' % filename)

                commands_output = 'set automatic_html_opening False\n'
                commands_output += 'pythia8 %s\n1\n0\n' % run_name

                with open('%s/run_%s.txt' % (output_dir, filename), 'w') as f:
                    f.write(commands_output)

                print('Done: %s\n' % filename)
