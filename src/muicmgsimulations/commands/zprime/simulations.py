from muicmgsimulations.commands.zprime.base import model_commands, base_config
from muicmgsimulations.commands.zprime.parameters import calc_g_mu, calc_g_b
from muicmgsimulations.execution import runtime
from muicmgsimulations.utils.constants import machines

processes = [
    {'name': 'zp_sb_to_b', 'title': 'BSM Z\' s,b to b',
     'commands': ['define p = b b~ s s~', 'define j = b b~', 'generate p mu- > j mu- BSM<=3 QED==0'],
     'model': '5-zp'},
    {'name': 'nc_bj_intf', 'title': 'SM NC to b-jet Interference Terms',
     'commands': ['define p = b b~', 'define j = b b~', 'generate p mu- > j mu- BSM^2<=2 QED^2<=2'],
     'model': '5-zp'},
    {'name': 'nc_bj', 'title': 'SM NC to b-jet',
     'commands': ['define p = b b~', 'define j = b b~', 'generate p mu- > j mu- BSM==0 QED==2'],
     'model': '5-zp'},
    {'name': 'nc_cj', 'title': 'SM NC to c-jet',
     'commands': ['define p = c c~', 'define j = c c~', 'generate p mu- > j mu- BSM==0 QED==2'],
     'model': '5-zp'},
    {'name': 'nc_lj', 'title': 'SM NC to l-jet',
     'commands': ['define p = u u~ d d~ s s~', 'define j = u u~ d d~ s s~', 'generate p mu- > j mu- BSM==0 QED==2'],
     'model': '5-zp'},
]


def generate_process_generators(output_dir):
    # GENERATE PROCESSES
    for process in processes:
        print('Generating commands for %s' % process['title'])

        commands = list()

        # APPEND FLAVOR SUPPORT
        commands.extend(model_commands.get(process['model'], list()))

        # APPEND PROCESS
        commands.extend(process['commands'])

        # APPEND OUTPUT
        commands.append('output %s\n' % process['name'])

        # BUILD INPUT
        commands_output = ''

        for command in commands:
            commands_output += '%s%s' % ('' if commands_output == '' else '\n', command)

        with open('%s/generate_process_%s.txt' % (output_dir, process['name']), 'w') as f:
            f.write(commands_output)

        print('Done: %s\n' % process['name'])


def generate_param_scan_runs(output_dir):
    process_list = ['zp_sb_to_b', 'nc_bj_intf']
    machine_list = ['MuIC', 'MuIC2', 'LHmuC']

    for machine_name in machine_list:
        machine = machines[machine_name]

        for process_name in process_list:
            masses = [200, 350, 500, 1000, 3000, 10000]

            for mass in masses:
                delbs_list = [0.01, 0.02, 0.03, 0.05, 0.07, 0.1, 0.5, 1, 10, 100]

                for delbs in delbs_list:
                    gmu = calc_g_mu(mass)
                    gb = calc_g_b(delbs, gmu, mass)

                    run_name = '%s_mz_%sGeV_delbs_%s' % (
                        machine_name, mass, str(delbs).replace('.', 'p'))
                    filename = 'param_scan_%s_%s_mz_%sGeV_delbs_%s' % (
                        process_name, machine_name, mass, str(delbs).replace('.', 'p'))

                    print('Generating %s' % filename)

                    commands_output = 'set automatic_html_opening False\n'
                    commands_output += 'launch %s -n %s\n0\n' % (process_name, run_name)

                    config = base_config.copy()
                    config['iseed'] = runtime.seeds.next()
                    config['nevents'] = 600000
                    config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
                    config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
                    config['ebeam1'] = machine['Ep']
                    config['ebeam2'] = machine['Emu']
                    config['polbeam1'] = 0
                    config['polbeam2'] = 0
                    config['dynamical_scale_choice'] = 0
                    # Z'
                    config['MZp'] = '%d' % mass  # GeV
                    config['gmu'] = '{:e}'.format(gmu)
                    config['gtau'] = '{:e}'.format(0)
                    config['gb'] = '{:e}'.format(gb)
                    config['delbs'] = '{:e}'.format(delbs)

                    for key, value in config.items():
                        commands_output += 'set %s %s\n' % (key, value)

                    commands_output += '0\n'

                    with open('%s/run_%s.txt' % (output_dir, filename), 'w') as f:
                        f.write(commands_output)

                    print('Done: %s\n' % filename)


def generate_comp_runs(output_dir):
    process_list = ['zp_sb_to_b', 'nc_bj', 'nc_bj_intf', 'nc_cj', 'nc_lj']

    combination_list = [
        [['LHmuC'], [200, 350, 500], [0.1]],
        [['MuIC2'], [200, 350, 500], [0.1]],
        [['MuIC'], [200, 350, 500], [0.1]]
    ]

    for combination in combination_list:
        machine_list = combination[0]

        for machine_name in machine_list:
            machine = machines[machine_name]

            for process_name in process_list:
                mass_list = combination[1]

                for mass in mass_list:
                    delbs_list = combination[2]

                    for delbs in delbs_list:
                        gmu = calc_g_mu(mass)
                        gb = calc_g_b(delbs, gmu, mass)

                        run_name = '%s_mz_%sGeV_delbs_%s' % (
                            machine_name, mass, str(delbs).replace('.', 'p'))
                        filename = 'comp_%s_%s_mz_%sGeV_delbs_%s' % (
                            process_name, machine_name, mass, str(delbs).replace('.', 'p'))

                        print('Generating %s' % filename)

                        commands_output = 'set automatic_html_opening False\n'
                        commands_output += 'launch %s -n %s\n0\n' % (process_name, run_name)

                        config = base_config.copy()
                        config['iseed'] = runtime.seeds.next()
                        config['nevents'] = 600000
                        config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
                        config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
                        config['ebeam1'] = machine['Ep']
                        config['ebeam2'] = machine['Emu']
                        config['polbeam1'] = 0
                        config['polbeam2'] = 0
                        config['dynamical_scale_choice'] = 0
                        # Z'
                        config['MZp'] = '%d' % mass  # GeV
                        config['gmu'] = '{:e}'.format(gmu)
                        config['gtau'] = '{:e}'.format(0)
                        config['gb'] = '{:e}'.format(gb)
                        config['delbs'] = '{:e}'.format(delbs)

                        for key, value in config.items():
                            commands_output += 'set %s %s\n' % (key, value)

                        commands_output += '0\n'

                        with open('%s/run_%s.txt' % (output_dir, filename), 'w') as f:
                            f.write(commands_output)

                        print('Done: %s\n' % filename)


def generate_extp_runs(output_dir):
    process_list = ['zp_sb_to_b', 'nc_bj_intf']

    combination_list = [
        [['LHmuC'], [(500, 1.097404645617e-03, 1.480766467037e+00)]],
        [['MuIC2'], [(500, 2.811713036642e-03, 5.779394905608e-01)]],
        [['MuIC'], [(500, 5.271717604134e-03, 3.082486813644e-01)]]
    ]

    for combination in combination_list:
        machine_list = combination[0]

        for machine_name in machine_list:
            machine = machines[machine_name]

            for process_name in process_list:
                ext_params = combination[1]

                for param_tup in ext_params:
                    mass, gb, delbs = param_tup

                    gmu = calc_g_mu(mass)

                    run_name = '%s_mz_%sGeV_delbs_%s' % (
                        machine_name, mass, str(delbs).replace('.', 'p'))
                    filename = 'ext_%s_%s_mz_%sGeV_delbs_%s' % (
                        process_name, machine_name, mass, str(delbs).replace('.', 'p'))

                    print('Generating %s' % filename)

                    commands_output = 'set automatic_html_opening False\n'
                    commands_output += 'launch %s -n %s\n0\n' % (process_name, run_name)

                    config = base_config.copy()
                    config['iseed'] = runtime.seeds.next()
                    config['nevents'] = 600000
                    config['lpp1'] = 1  # 0=NOPDF, 1=PROTON
                    config['lpp2'] = 0  # 0=NOPDF, 1=PROTON
                    config['ebeam1'] = machine['Ep']
                    config['ebeam2'] = machine['Emu']
                    config['polbeam1'] = 0
                    config['polbeam2'] = 0
                    config['dynamical_scale_choice'] = 0
                    # Z'
                    config['MZp'] = '%d' % mass  # GeV
                    config['gmu'] = '{:e}'.format(gmu)
                    config['gtau'] = '{:e}'.format(0)
                    config['gb'] = '{:e}'.format(gb)
                    config['delbs'] = '{:e}'.format(delbs)

                    for key, value in config.items():
                        commands_output += 'set %s %s\n' % (key, value)

                    commands_output += '0\n'

                    with open('%s/run_%s.txt' % (output_dir, filename), 'w') as f:
                        f.write(commands_output)

                    print('Done: %s\n' % filename)
