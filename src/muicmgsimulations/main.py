import argparse

from muicmgsimulations.commands.higgs import command as higgs_command
from muicmgsimulations.commands.leptoquark import command as lq_command
from muicmgsimulations.commands.nc import command as nc_command
from muicmgsimulations.commands.zprime import command as zprime_command
from muicmgsimulations.execution import runtime
from muicmgsimulations.utils.seeds import SeedRepository


def main():
    parser = argparse.ArgumentParser(prog=runtime.project_name, description='MUIC Madgraph Simulations')

    parser.add_argument('--seed', dest='seed',
                        metavar='SEED', type=int, default=100,
                        help='Initial seed used to generate scripts. '
                             'The seed is used to create consecutively-unique seeds for each run script '
                             'e.g. 100 101 102, ... 100 + N')

    parser.add_argument('--output', dest='output_dir',
                        metavar='TARGET_DIR', type=str, default=None,
                        help='Directory where all the scripts will be stored, if it doesn\'t exist, it will be created.')

    subparsers = parser.add_subparsers(dest='parser', metavar="COMMAND", help='Command to be run')

    # CONFIGURE COMMANDS
    command_entries = {
        'nc': ('Generates NC Simulations MG5 Files', nc_command),
        'higgs': ('Generates Higgs Simulation MG5 Files', higgs_command),
        'zprime': ('Generates Z\' Simulations MG5 Files', zprime_command),
        'lq': ('Generates LQ Simulations MG5 Files', lq_command),
    }

    for key, command_entry in command_entries.items():
        subparser = subparsers.add_parser(key, help=command_entry[0])
        command_entry[1].configure_parser(subparser)

    args = parser.parse_args()

    # RUN COMMAND
    command_entry = command_entries.get(args.parser)

    if command_entry is None:
        parser.print_help()
        exit(1)

    runtime.output_dir = (args.parser if args.output_dir is None else args.output_dir)
    runtime.seeds = SeedRepository(args.seed)

    command_entry[1].run(args)
